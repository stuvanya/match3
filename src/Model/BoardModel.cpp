#include "BoardModel.h"

BoardModel::BoardModel(int x, int y, int max):
    _x(x),
    _y(y),
    _max(max),
    _matrixChanged(false),
    _score(0)
{
    newGame();
}

GameMatrix BoardModel::getGameMatrix() const
{
    return _gameMatrix;
}

GameMatrix BoardModel::getPrevMatrix() const
{
    return _prevMatrix;
}

void BoardModel::setGameMatrix(const GameMatrix &gameMatrix)
{
    _gameMatrix = gameMatrix;
}

GameMatrix BoardModel::generateGameMatrix()
{
    _score = 0;
    srand(time(0));
    GameMatrix matrix;
    for (int i = 0; i < _y; i++)
    {
        vector<Cell> row;
        for (int j = 0; j < _x; j++)
        {
            int cell = 1 + rand() % _max;
            while (true)
            {
                if (((j >= 2) && row.at(j - 1).value == row.at(j - 2).value
                     && row.at(j - 1).value == cell)
                        || ((i >= 2) && matrix.at(i - 1).at(j).value == matrix.at(i - 2).at(j).value
                            && matrix.at(i - 1).at(j).value == cell))
                    cell = 1 + rand() % _max;
                else
                    break;
            }
            row.push_back(Cell(cell));
        }
        matrix.push_back(row);
    }
    return matrix;
}

void BoardModel::matchMatrix()
{
    if (checkMatrix(_gameMatrix))
    {
        for (int i = 0; i < _y; i++)
        {
            for (int j = 0; j < _x; j++)
            {
                if ((j >= 2) && _gameMatrix.at(i).at(j - 1).value == _gameMatrix.at(i).at(j - 2).value
                        && _gameMatrix.at(i).at(j - 1).value == _gameMatrix.at(i).at(j).value)
                {
                    if (!_gameMatrix.at(i).at(j - 1).destroy)
                        _score++;
                    if (!_gameMatrix.at(i).at(j - 2).destroy)
                        _score++;
                    if (!_gameMatrix.at(i).at(j).destroy)
                        _score++;
                    _gameMatrix.at(i).at(j - 1).destroy = true;
                    _gameMatrix.at(i).at(j - 2).destroy = true;
                    _gameMatrix.at(i).at(j).destroy = true;
                }
                else if ((i >= 2) && _gameMatrix.at(i - 1).at(j).value == _gameMatrix.at(i - 2).at(j).value
                         && _gameMatrix.at(i - 1).at(j).value == _gameMatrix.at(i).at(j).value)
                {
                    if (!_gameMatrix.at(i).at(j).destroy)
                        _score++;
                    if (!_gameMatrix.at(i - 1).at(j).destroy)
                        _score++;
                    if (!_gameMatrix.at(i - 2).at(j).destroy)
                        _score++;
                    _gameMatrix.at(i - 1).at(j).destroy = true;
                    _gameMatrix.at(i - 2).at(j).destroy = true;
                    _gameMatrix.at(i).at(j).destroy = true;
                }
            }
        }
        _matrixChanged = true;
        _prevMatrix = _gameMatrix;
        moveMatrix();
    }
}

pair<Point, Point> BoardModel::getHelp()
{
    for (int i = 0; i < _y; i++)
    {
        for (int j = 0; j < _x; j++)
        {
            if (i + 1 != _y)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i + 1).at(j);
                newMatrix.at(i + 1).at(j) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                if (checkMatrix(newMatrix))
                {
                    return make_pair(Point(j, i), Point(j, i + 1));
                }
            }

            if (i != 0)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i - 1).at(j);
                newMatrix.at(i - 1).at(j) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                if (checkMatrix(newMatrix))
                {
                    return make_pair(Point(j, i), Point(j, i - 1));
                }
            }

            if (j != 0)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i).at(j - 1);
                newMatrix.at(i).at(j - 1) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                if (checkMatrix(newMatrix))
                {
                    return make_pair(Point(j, i), Point(j - 1, i));
                }
            }

            if (j + 1 != _x)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i).at(j + 1);
                newMatrix.at(i).at(j + 1) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                if (checkMatrix(newMatrix))
                {
                    return make_pair(Point(j, i), Point(j + 1, i));
                }
            }
        }
    }
    return make_pair(Point(0, 0), Point(0, 1));
}

void BoardModel::moveMatrix()
{
    for (int i = 0; i < _y; i++)
    {
        for (int j = 0; j < _x; j++)
        {
            if (_gameMatrix.at(i).at(j).destroy)
            {
                for (int l = i; l >= 0; l--)
                {
                    if (l != 0)
                        _gameMatrix.at(l).at(j) = _gameMatrix.at(l - 1).at(j);
                    else
                        _gameMatrix.at(l).at(j) = Cell(1 + rand() % _max);
                }
            }
        }
    }
}

void BoardModel::setMatrixChanged(bool matrixChanged)
{
    _matrixChanged = matrixChanged;
}

int BoardModel::getScore() const
{
    return _score;
}

bool BoardModel::checkGameEnd() const
{
    bool havePossibilites = false;
    for (int i = 0; i < _y; i++)
    {
        for (int j = 0; j < _x; j++)
        {
            if (i + 1 != _y)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i + 1).at(j);
                newMatrix.at(i + 1).at(j) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                havePossibilites = checkMatrix(newMatrix);
                if (havePossibilites) return false;
            }

            if (i != 0)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i - 1).at(j);
                newMatrix.at(i - 1).at(j) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                havePossibilites = checkMatrix(newMatrix);
                if (havePossibilites) return false;
            }

            if (j != 0)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i).at(j - 1);
                newMatrix.at(i).at(j - 1) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                havePossibilites = checkMatrix(newMatrix);
                if (havePossibilites) return false;
            }

            if (j + 1 != _x)
            {
                auto newMatrix = _gameMatrix;
                auto a = newMatrix.at(i).at(j + 1);
                newMatrix.at(i).at(j + 1) = newMatrix.at(i).at(j);
                newMatrix.at(i).at(j) = a;
                havePossibilites = checkMatrix(newMatrix);
                if (havePossibilites) return false;
            }
        }
    }
    return !havePossibilites;
}

bool BoardModel::istMatrixChanged() const
{
    return _matrixChanged;
}

int BoardModel::getMaxValue() const
{
    return _max;
}

bool BoardModel::checkMatrix(GameMatrix matrix) const
{
    for (int i = 0; i < _y; i++)
    {
        vector<Cell> row = matrix.at(i);
        for (int j = 0; j < _x; j++)
        {
            if (((j >= 2) && row.at(j - 1).value == row.at(j - 2).value
                 && row.at(j - 1).value == row.at(j).value)
                    || ((i >= 2) && matrix.at(i - 1).at(j).value == matrix.at(i - 2).at(j).value
                        && matrix.at(i - 1).at(j).value == row.at(j).value))
                return true;
        }
    }
    return false;
}

bool BoardModel::checkCells(Point pos1, Point pos2) const
{
    auto newMatrix = _gameMatrix;
    auto a = newMatrix.at(pos2.y).at(pos2.x);
    newMatrix.at(pos2.y).at(pos2.x) = newMatrix.at(pos1.y).at(pos1.x);
    newMatrix.at(pos1.y).at(pos1.x) = a;
    return _gameMatrix.at(pos1.y).at(pos1.x).value !=_gameMatrix.at(pos2.y).at(pos2.x).value
            && checkMatrix(newMatrix);
}

void BoardModel::swapCells(Point pos1, Point pos2)
{
    auto a = _gameMatrix.at(pos2.y).at(pos2.x);
    _gameMatrix.at(pos2.y).at(pos2.x) = _gameMatrix.at(pos1.y).at(pos1.x);
    _gameMatrix.at(pos1.y).at(pos1.x) = a;
    matchMatrix();
}

void BoardModel::newGame()
{
    _score = 0;
    _gameMatrix.clear();
    _gameMatrix = generateGameMatrix();
    while (checkGameEnd())
        _gameMatrix = generateGameMatrix();
}

int BoardModel::getYSize() const
{
    return _y;
}

int BoardModel::getXSize() const
{
    return _x;
}
