#pragma once
#include "oxygine-framework.h"
#include <set>
#include <utility>

using namespace oxygine;
using namespace std;

struct Cell
{
    int value;
    bool destroy;
    Cell(int value) :
        value(value),
        destroy(false)
    {}
};

using GameMatrix = vector<vector<Cell>>;

class BoardModel
{
public:
    BoardModel(int x = 10, int y = 10, int max = 6);
    GameMatrix getGameMatrix() const;
    GameMatrix getPrevMatrix() const;
    void setGameMatrix(const GameMatrix &gameMatrix);

    int getXSize() const;
    int getYSize() const;
    int getMaxValue() const;

    bool checkMatrix(GameMatrix matrix) const;
    bool checkCells(Point pos1, Point pos2) const;
    void swapCells(Point pos1, Point pos2);
    void newGame();

    bool istMatrixChanged() const;

    int getScore() const;
    bool checkGameEnd() const;
    void setMatrixChanged(bool matrixChanged);
    void matchMatrix();
    pair<Point, Point> getHelp();

private:
    GameMatrix _gameMatrix, _prevMatrix;
    GameMatrix generateGameMatrix();
    void moveMatrix();
    const int _x, _y, _max;
    bool _matrixChanged;
    int _score;
};
