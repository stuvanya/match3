#pragma once
#include "oxygine-framework.h"
#include "../Model/BoardModel.h"
#include "../View/BoardView.h"

using namespace oxygine;
using namespace std;

enum class State
{
    IDLE,
    BUSY
};

class BoardPresenter
{
public:
    BoardPresenter(BoardModel* model);
    void show(spActor parent);
    void hide();
private:
    void setMatrix();
    void cellClicked(Point pos);
    void swapCells(Point pos1, Point pos2);
    void hideRemovedCells();
    void gameEnd();
    void newGame();
    Point _prevClickedCell, _currentClickedCell;
    spBoardView _view;
    BoardModel* _model;
    State _currentState;
    void checkResult();
    void fallCells();
    void showHelp();
};
