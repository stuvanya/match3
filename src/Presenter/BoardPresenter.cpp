#include "BoardPresenter.h"

BoardPresenter::BoardPresenter(BoardModel *model) :
    _prevClickedCell(Point(-1, -1)),
    _currentClickedCell(Point(-1, -1)),
    _view(new BoardView(Point(model->getXSize(), model->getYSize()), model->getMaxValue())),
    _model(model),
    _currentState(State::IDLE)
{
    setMatrix();

    for (int i = 0; i < _model->getYSize(); i++)
    {
        for (int j = 0; j < _model->getXSize(); j++)
        {
            _view->gameMatrix.at(i).at(j)->addClickListener([this, j, i](Event*){
                cellClicked(Point(j, i));
            });
        }
    }

    _view->newGameButton->addClickListener([this](Event*){
        if (_currentState == State::IDLE)
        {
            _currentState = State::BUSY;
            _model->newGame();
            newGame();
        }
    });

    _view->endNewGameButton->addClickListener([this](Event*){
        _currentState = State::BUSY;
        _view->endGameActor->setVisible(false);
        _model->newGame();
        newGame();
    });

    _view->helpButton->addClickListener([this](Event*){
        showHelp();
    });

    _currentState = State::IDLE;
}

void BoardPresenter::show(spActor parent)
{
    _view->setScale(parent->getHeight() / _view->getHeight());
    _view->setPosition(parent->getSize() / 2 - _view->getScaledSize() / 2);
    parent->addChild(_view);

}

void BoardPresenter::hide()
{
    _view->detach();
}

void BoardPresenter::setMatrix()
{
    _currentState = State::BUSY;
    auto matrix = _model->getGameMatrix();
    for (int i = 0; i < _model->getYSize(); i++)
    {
        for (int j = 0; j < _model->getXSize(); j++)
        {
            if (!matrix.at(i).at(j).destroy)
            {
                _view->gameMatrix.at(i).at(j)->setAlpha(255);
                _view->gameMatrix.at(i).at(j)->setResAnim(_view->diamondRes.at(matrix.at(i).at(j).value - 1));
                _view->gameMatrix.at(i).at(j)->removeTweens();
                _view->gameMatrix.at(i).at(j)->setRotationDegrees(0);
            }
            else
            {
                _view->gameMatrix.at(i).at(j)->setVisible(false);
            }
        }
    }
}

void BoardPresenter::cellClicked(Point pos)
{
    if (pos == _currentClickedCell || _currentState != State::IDLE)
        return;

    if (_prevClickedCell.x != -1)
    {
        auto prevPrevCell = _view->gameMatrix.at(_prevClickedCell.y).at(_prevClickedCell.x);
        prevPrevCell->removeTweens();
        prevPrevCell->setRotationDegrees(0);
    }
    if (_currentClickedCell.x != -1)
    {
        _prevClickedCell = _currentClickedCell;
    }
    _currentClickedCell = pos;
    auto cell = _view->gameMatrix.at(_currentClickedCell.y).at(_currentClickedCell.x);
    cell->removeTweens();
    cell->setRotationDegrees(-5);
    cell->addTween(Sprite::TweenRotationDegrees(10), TweenOptions(200).twoSides(true).loops(-1).ease(Tween::ease_inOutSin));

    if (_currentClickedCell.distance(_prevClickedCell) <= 1
            && (_currentClickedCell.x == _prevClickedCell.x || _currentClickedCell.y == _prevClickedCell.y)
            && _model->checkCells(_currentClickedCell, _prevClickedCell))
    {
        swapCells(_prevClickedCell, _currentClickedCell);
        _currentState = State::BUSY;
    }
    else
    {
        if (_prevClickedCell.x != -1)
        {
            auto prevPrevCell = _view->gameMatrix.at(_prevClickedCell.y).at(_prevClickedCell.x);
            prevPrevCell->removeTweens();
            prevPrevCell->setRotationDegrees(0);
            _prevClickedCell = Point(-1, -1);
        }
    }
}

void BoardPresenter::swapCells(Point pos1, Point pos2)
{
    _model->swapCells(pos1, pos2);
    auto cell1 = _view->gameMatrix.at(pos1.y).at(pos1.x);
    auto cell2 = _view->gameMatrix.at(pos2.y).at(pos2.x);

    auto pos1v = cell1->getPosition();
    auto pos2v = cell2->getPosition();

    cell1->removeTweens();
    cell2->removeTweens();

    cell1->setPosition(pos2v);
    cell2->setPosition(pos1v);
    auto res1 = cell1->getResAnim();
    cell1->setResAnim(cell2->getResAnim());
    cell2->setResAnim(res1);

    cell2->addTween(Actor::TweenPosition(pos2v), TweenOptions(200).ease(Tween::ease_inOutSin));
    cell1->addTween(Actor::TweenPosition(pos1v), TweenOptions(200).ease(Tween::ease_inOutSin))->addDoneCallback([this](Event*){
        auto prevPrevCell = _view->gameMatrix.at(_prevClickedCell.y).at(_prevClickedCell.x);
        prevPrevCell->removeTweens();
        prevPrevCell->setRotationDegrees(0);

        auto cell = _view->gameMatrix.at(_currentClickedCell.y).at(_currentClickedCell.x);
        cell->removeTweens();
        cell->setRotationDegrees(0);

        _currentClickedCell = Point(-1, -1);
        _prevClickedCell = Point(-1, -1);

        hideRemovedCells();
    });
}

void BoardPresenter::hideRemovedCells()
{
    auto prevMatrix = _model->getPrevMatrix();
    for (int i = 0; i < _model->getYSize(); i++)
    {
        for (int j = 0; j < _model->getXSize(); j++)
        {
            if (prevMatrix.at(i).at(j).destroy)
            {
                auto cell = _view->gameMatrix.at(i).at(j);
                cell->addTween(Sprite::TweenRotationDegrees(20), TweenOptions(100).twoSides(true).loops(5).ease(Tween::ease_inOutSin));
                cell->addTween(Actor::TweenAlpha(0), 500);

                spSprite boom = new Sprite();
                boom->setScale(cell->getScale());
                boom->setResAnim(_view->boomAnim);
                boom->setAnchor(0.5f, 0.5f);
                boom->setPosition(cell->getPosition());
                boom->attachTo(_view->bg);
                boom->addTween(TweenAnim(boom->getResAnim()), 500)->addDoneCallback([boom](Event*){
                    boom->detach();
                });
            }
        }
    }
    _view->addTween(TweenDummy(), 500)->addDoneCallback([this](Event*){
        _model->setMatrixChanged(false);
        fallCells();
    });
}

void BoardPresenter::fallCells()
{
    auto prevMatrix = _model->getPrevMatrix();
    auto newMatrix = _model->getGameMatrix();
    int moveDuration = 120;
    int maxMoveDuration =  _model->getYSize() * moveDuration;

    for (int x = 0; x < _model->getXSize(); x++)
    {
        int move = 0;
        for (int y = _model->getYSize() - 1; y >= 0; y--)
        {
            if (prevMatrix.at(y).at(x).destroy)
            {
                move ++;
            }
            if (move > 0)
            {
                auto cell = _view->gameMatrix.at(y).at(x);

                cell->setAlpha(255);
                cell->setVisible(true);

                auto prevPos = cell->getPosition();
                cell->setY(cell->getY() - move * (600 / _model->getYSize()));
                cell->setResAnim(_view->diamondRes.at(newMatrix.at(y).at(x).value - 1));
                cell->addTween(Actor::TweenPosition(prevPos), TweenOptions(move * moveDuration).ease(Tween::ease_inSin));
            }
        }
    }

    _view->addTween(TweenDummy(), maxMoveDuration)->addDoneCallback([this](Event*){
        checkResult();
    });
}

void BoardPresenter::checkResult()
{
    setMatrix();
    if (_model->checkMatrix(_model->getGameMatrix()))
    {
        _model->matchMatrix();
        hideRemovedCells();
    }
    else if (_model->checkGameEnd())
    {
        _view->addTween(TweenDummy(), 500)->addDoneCallback([this](Event*){
            gameEnd();
        });
    }
    else
    {
        _currentState = State::IDLE;
    }
}

void BoardPresenter::gameEnd()
{
    _view->endGameActor->setVisible(true);
}

void BoardPresenter::newGame()
{
    setMatrix();
    _currentState = State::IDLE;
}

void BoardPresenter::showHelp()
{
    auto help = _model->getHelp();
    _view->gameMatrix.at(help.first.y).at(help.first.x)->addTween(Sprite::TweenRotationDegrees(10), TweenOptions(200).twoSides(true).loops(10).ease(Tween::ease_inOutSin));
    _view->gameMatrix.at(help.second.y).at(help.second.x)->addTween(Sprite::TweenRotationDegrees(10), TweenOptions(200).twoSides(true).loops(10).ease(Tween::ease_inOutSin));
}
