#pragma once
#include "oxygine-framework.h"

using namespace oxygine;
using namespace std;

DECLARE_SMART(BoardView, spBoardView)

class BoardView : public Actor
{
public:
    BoardView(Point size, int max);
    ~BoardView();
    Resources res;
    spSprite bg;
    vector<ResAnim*> diamondRes;
    ResAnim* boomAnim;
    vector<vector<spSprite>> gameMatrix;
    spButton newGameButton, helpButton;
    spActor endGameActor;
    spButton endNewGameButton;
};
