#include "BoardView.h"

BoardView::BoardView(Point size, int max)
{
    setSize(600, 700);
    res.loadXML("data/resources.xml");
    bg = new Sprite;
    bg->setResAnim(res.getResAnim("bg"));
    bg->attachTo(this);

    for (int i = 1; i <= max; i++)
    {
        diamondRes.push_back(res.getResAnim(to_string(i)));
    }

    for (int i = 0; i < size.y; i++)
    {
        vector<spSprite> row;
        for (int j = 0; j < size.x; j++)
        {
            spSprite diamond = new Sprite();
            diamond->setResAnim(res.getResAnim("1"));
            diamond->setScale((600 / size.x) / diamond->getWidth());
            diamond->attachTo(bg);
            diamond->setAnchor(0.5f, 0.5f);
            diamond->setPosition((j + 0.5f) * (600 / size.x), 100 + (i + 0.5f) * (600 / size.y));
            row.push_back(diamond);
        }
        gameMatrix.push_back(row);
    }

    newGameButton = new Button();
    newGameButton->setResAnim(res.getResAnim("button"));
    newGameButton->setPosition(Vector2(20, 20));
    newGameButton->attachTo(bg);

    helpButton = new Button();
    helpButton->setResAnim(res.getResAnim("button"));
    helpButton->setPosition(Vector2(300, 20));
    helpButton->attachTo(bg);

    endGameActor = new Actor();
    endGameActor->setVisible(false);
    endGameActor->setSize(getSize());
    endGameActor->attachTo(bg);

    auto colorSpriteBg = new ColorRectSprite();
    colorSpriteBg->setColor(Color::Black);
    colorSpriteBg->setAlpha(128);
    colorSpriteBg->setSize(getSize());
    colorSpriteBg->attachTo(endGameActor);

    auto endSprite = new Sprite();
    endSprite->setResAnim(res.getResAnim("gameOver"));
    endSprite->setPosition(150, 250);
    endSprite->attachTo(endGameActor);

    endNewGameButton = new Button();
    endNewGameButton->setResAnim(res.getResAnim("button"));
    endNewGameButton->setPosition(Vector2(40, 90));
    endNewGameButton->attachTo(endSprite);

    boomAnim = res.getResAnim("boom");
}

BoardView::~BoardView()
{
    removeTweens();
    removeChildren();
}
