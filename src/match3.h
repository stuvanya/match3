#pragma once
#include "oxygine-framework.h"
#include "Presenter/BoardPresenter.h"
#include "Model/BoardModel.h"
using namespace oxygine;
void preinit();
void init(int board_size_x, int board_size_y);
void destroy();
void update();

