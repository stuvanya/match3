#include "match3.h"
#include "Presenter/BoardPresenter.h"
#include "Model/BoardModel.h"

using namespace oxygine;
Resources res;

void preinit()
{}

void init(int board_size_x, int board_size_y)
{
    BoardModel* model = new BoardModel(board_size_x, board_size_y, 6);
    BoardPresenter* presenter = new BoardPresenter(model);
    presenter->show(getStage());
}

void destroy()
{
    res.free();
}
void update()
{

}
